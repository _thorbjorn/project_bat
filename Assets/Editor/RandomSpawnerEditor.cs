﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RandomSpawner))]
public class RandomSpawnerEditor : Editor
{
    private List<GameObject> m_Trees = new List<GameObject>();

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        RandomSpawner spawner = (RandomSpawner)target;
        if (GUILayout.Button("Spawn Obstacles"))
        {
            var trees = spawner.TestSpawn(Vector3.forward * spawner.ZVariance);

            for (int i = trees.Length - 1; i >= 0; i--)
            {
                m_Trees.Add(trees[i]);
            }
        }

        if (GUILayout.Button("Clear Obstacles"))
        {
            Clear();
        }
    }

    private void OnDisable()
    {
        Clear();
    }

    private void Clear()
    {
        for (int i = m_Trees.Count - 1; i >= 0; i--)
        {
            DestroyImmediate(m_Trees[i]);
        }
        m_Trees.Clear();
    }
}
