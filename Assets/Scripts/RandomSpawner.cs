﻿using System.Collections.Generic;
using UnityEngine;


public class RandomSpawner : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private GameObject[] m_Obastacles;
    [SerializeField] private GameObject m_Bat;
    [SerializeField] private int m_Count = 10;
    public float XVariance = 10f;
    public float ZVariance = 20f;
    public float ScaleRandomizer = 1.5f;

    private float m_CoolDown = 10;

    private int m_SpawnedCount = 0;

    private static RandomSpawner sInstance;

    private void Awake()
    {
        sInstance = this;
#if UNITY_EDITOR
        for (int i = 0; i < m_Obastacles.Length; i++)
        {
            Debug.Assert(m_Obastacles[i] != null, 
                "Random spawner contained invalid prefabs amongst the obstacle types", 
                this);
        }
#endif
    }

    private void Start()
    {
        Spawn(Vector3.forward * ZVariance);
        Spawn(Vector3.forward * ZVariance * 2);
        m_SpawnedCount = 0;
    }

    private void Update()
    {
        m_CoolDown += Time.deltaTime;
    }

    public static void Spawn()
    {
        if (sInstance.m_CoolDown < 2.5f)
        {
            return;
        }
        sInstance.Spawn(Vector3.forward * sInstance.ZVariance * ++sInstance.m_SpawnedCount * 2);
        sInstance.m_CoolDown = 0;
    }

    public void Spawn(Vector3 fromPoint)
    {
        for (int i = 0; i < m_Count; i++)
        {
            var go = Instantiate(
                m_Obastacles[Random.Range(0, m_Obastacles.Length)], 
                new Vector3(
                    fromPoint.x + Random.Range(-XVariance, XVariance), 
                    fromPoint.y, 
                    fromPoint.z + Random.Range(-ZVariance, ZVariance)), 
                Quaternion.Euler(0, Random.Range(0f, 360f), 0),
                transform);

            go.transform.localScale = new Vector3(
                go.transform.localScale.x * Random.Range(1f, ScaleRandomizer), 
                go.transform.localScale.y * Random.Range(1f, ScaleRandomizer), 
                go.transform.localScale.z * Random.Range(1f, ScaleRandomizer));
        }
    }

#if UNITY_EDITOR
    public GameObject[] TestSpawn(Vector3 fromPoint)
    {
        GameObject[] trees = new GameObject[m_Count];
        for (int i = 0; i < m_Count; i++)
        {
            trees[i] = Instantiate(
                m_Obastacles[Random.Range(0, m_Obastacles.Length)],
                new Vector3(
                    fromPoint.x + Random.Range(-XVariance, XVariance),
                    fromPoint.y,
                    fromPoint.z + Random.Range(-ZVariance, ZVariance)),
                Quaternion.Euler(0, Random.Range(0f, 360f), 0),
                transform);

            trees[i].transform.localScale = new Vector3(
                trees[i].transform.localScale.x * Random.Range(1f, ScaleRandomizer),
                trees[i].transform.localScale.y * Random.Range(1f, ScaleRandomizer),
                trees[i].transform.localScale.z * Random.Range(1f, ScaleRandomizer));

        }

        return trees;
    }
#endif
}