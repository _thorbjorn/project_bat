﻿using UnityEngine;

public class LightBullet : MonoBehaviour
{
    [SerializeField] private Light m_Light;

    private void Start()
    {
        Destroy(gameObject, 5f);
    }

    private void Update()
    {
        m_Light.intensity *= 0.95f;
    }
}
