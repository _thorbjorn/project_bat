﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
#if UNITY_EDITOR
    private Collider m_ColliderCached;

    private void OnDrawGizmos()
    {
        if (m_ColliderCached == null)
        {
            m_ColliderCached = GetComponent<Collider>();
        }

        Gizmos.color = new Color(1f, 0.5f, 0f, 1f);
        Gizmos.DrawWireCube(m_ColliderCached.bounds.center, m_ColliderCached.bounds.size);
    }
#endif
}