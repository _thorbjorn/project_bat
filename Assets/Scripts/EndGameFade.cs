﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameFade : MonoBehaviour
{
    [SerializeField] private Material material;
    [SerializeField]  private Canvas m_Canvas;

     private void Reset()
    {
        material.color = new Color(0, 0, 0, 1);
    }

    private void OnDisable()
    {
        Reset();
    }

    private void Update()
    {
        var distance = transform.position.z - Camera.main.transform.position.z;
        if (distance < 110f)
        {
            if (distance < 0.1f)
            {
                Camera.main.gameObject.GetComponent<AudioSource>().enabled = false;
                m_Canvas.gameObject.SetActive(true);

                if (distance < -100f)
                {
                    SceneManager.LoadScene(0, LoadSceneMode.Single);
                }
                return;
            }

            var alpha = Mathf.Clamp01((distance - 10f) / 100f);
            material.color = new Color(0, 0, 0, alpha);

        }
    }
}