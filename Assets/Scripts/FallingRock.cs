﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingRock : MonoBehaviour {

private bool startFalling;
public bool StartFalling
    {
        get
        {return startFalling;}

        set
        {startFalling = value;}
    }

     [SerializeField] Rigidbody rBody;
	 [SerializeField] private AudioSource audioFalling;
     public float speed = 1;
     public float amplitude = 2;

     private bool firstTry = true;

     private Vector3 vec3Force;

	 private float rumbleTimeStart;


    void Start()
    {
    	Vector3 origin = transform.position;
    }
    private void Awake()
    {
    	Debug.Assert(rBody != null, "The Bat is missing a reference to its rigidbody", this);
    	rBody.useGravity = false;
    }


     void FixedUpdate ()
     {
    	 if(StartFalling){
			 
            if (firstTry){  
    	 	vec3Force = generateRandomVector(amplitude);
    		rBody.AddForce(vec3Force, ForceMode.VelocityChange);
    		firstTry = false;
    		}
    		else{		
    	 	rBody.AddForce(-vec3Force, ForceMode.VelocityChange);
    	 	firstTry = true;
    	 	}
			if(Time.time > rumbleTimeStart+1.5f){
			StartFalling = false;
			rBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
			rBody.velocity = new Vector3(0,0,0);	
    		rBody.useGravity = true;
			audioFalling.Play();
			
			}
    	 }
     }

     Vector3 generateRandomVector(float amp)
     {
         Vector3 result = new Vector3();
         for (int i = 0; i < 3; i++)
         {
             float x = Random.Range(-amp, amp);
             result[i] = x;
         }
         return result;
     }

     private void OnTriggerEnter(Collider col)
        {	
            if(col.tag == "Player")
            {
    			StartFalling = true;
				rumbleTimeStart = Time.time;
				
            }
    	}


}
