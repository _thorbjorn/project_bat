﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFloat : MonoBehaviour {

	[SerializeField] float floatAmplitude;
	[SerializeField] float speedUp = 3f;

	void Start () {
	}


	void Update()
	{
		transform.Translate(new Vector3(0,floatAmplitude * Mathf.Sin(speedUp * Time.time),0));
	}

}
