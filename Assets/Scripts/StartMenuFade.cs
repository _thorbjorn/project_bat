﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuFade : MonoBehaviour {

	[SerializeField] private bool GameStarted = false;
	[SerializeField] private GameObject mainMenu;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GameStarted){
			return;
		}else{
			if(Input.GetMouseButtonDown(0)){
				EnterGame();
			}
		}
	}

	public void EnterGame(){
    mainMenu.SetActive(false);
	GameStarted = true;
    }
}
