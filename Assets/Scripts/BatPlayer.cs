﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BatPlayer : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private GameObject m_LightBullet;
    [SerializeField] private Rigidbody m_Body;
    [SerializeField] private AudioClip[] m_Screeches;
    [SerializeField] private AudioClip m_HitSound;
    [SerializeField] private AudioSource m_SchreechSource;
    [SerializeField] private float upThrust;
    [SerializeField] private float m_TurnSpeedX = 10;
    [SerializeField] private float m_TurnSpeedY = 12;
    [SerializeField] private float m_MoveSpeed3 = 12;
    [SerializeField] private float m_MoveSpeed2 = 10;
    [SerializeField] private float m_MoveSpeed1 = 8;
    [SerializeField] private float m_InvurnablityTime = 1.5f;
    [SerializeField] private float m_InputDownScale = 1;

    [SerializeField]
    private FollowBat mainCamera;
    [SerializeField]
    private TeethFollow teeth;


    private float m_LastInvul;
    private int m_Health = 3;
    private float m_MoveSpeed;
    private Vector3 m_LastMousePos;
    private Vector3 m_DeltaMouse;

    private void Start()
    {
        m_MoveSpeed = m_MoveSpeed3;
        teeth.ChangeDist(3.0f);
        m_LastInvul = Time.time;
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "LightWall")
        {
            m_SchreechSource.clip = m_Screeches[Random.Range(0, m_Screeches.Length)];
            m_SchreechSource.pitch = Random.Range(0.8f, 1.2f);
            m_SchreechSource.Play();
            Instantiate(m_LightBullet, transform.position, Quaternion.identity);
        }

        if (col.tag == "Obstacle")
        {
            if (m_LastInvul > Time.time)
            {
                return;
            }
            m_SchreechSource.PlayOneShot(m_HitSound);
            m_LastInvul = Time.time + m_InvurnablityTime;
            m_Health--;
            switch (m_Health)
            {
                case 2:
                    m_MoveSpeed = m_MoveSpeed2;
                    teeth.ChangeDist(-3.0f);
                    break;
                case 1:
                    m_MoveSpeed = m_MoveSpeed1;
                    break;
                case 0:
                    SceneManager.LoadScene(0, LoadSceneMode.Single);
                    break;
            }


            m_Body.AddForce(((Time.frameCount % 2) == 0 ? Vector3.left : Vector3.right) * 18, ForceMode.Impulse);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            m_DeltaMouse = (Input.mousePosition - m_LastMousePos) * m_InputDownScale * Time.deltaTime;
            if (m_DeltaMouse.magnitude > 1)
            {
                m_DeltaMouse.Normalize();
            }
        }
        m_Body.velocity += new Vector3(m_DeltaMouse.x * m_TurnSpeedX * Time.deltaTime, m_DeltaMouse.y * m_TurnSpeedY * Time.deltaTime);
        m_DeltaMouse *= 0.9f;
        Debug.DrawRay(transform.position, m_Body.velocity);
        m_Body.AddForce(Vector3.forward * m_MoveSpeed);
        m_LastMousePos = Input.mousePosition;
    }
}
