﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeethFollow : MonoBehaviour
{
    private float dynDist = 0.0f;
    [SerializeField]
    private AudioSource m_Source;
    [SerializeField]
    private AudioClip[] m_Clips;

    private float m_NextPlayTime = 0;

    // Update is called once per frame
    private void Update()
    {
        dynDist = 0.9f * dynDist;
        this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - dynDist);


        if (!m_Source.isPlaying && m_NextPlayTime < Time.time)
        {
            m_Source.clip = m_Clips[Random.Range(0, m_Clips.Length)];
            m_Source.pitch = Random.Range(0.8f, 1.2f);
            m_Source.Play();
            m_NextPlayTime = Time.time + m_Source.clip.length + Random.Range(1f, 6f);
        }

        if (transform.position.z > 500)
        {
            Destroy(gameObject);
        }
    }

    public void ChangeDist(float dist)
    {
        dynDist = dist;
    }
}