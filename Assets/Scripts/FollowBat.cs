﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBat : MonoBehaviour
{
    [SerializeField] private Transform m_BatTransform;
    [SerializeField] private float m_Distance = 6f;

    private void Update()
    {
        this.transform.position = new Vector3(0, 0, m_BatTransform.position.z - m_Distance);
    }
}