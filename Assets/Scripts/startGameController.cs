﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class startGameController : MonoBehaviour {

	private bool GameStarted = false;
	[SerializeField]
	private GameObject mainMenu;
	[SerializeField]
	private Animator anim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameStarted)
		{
			return;
		}
		else
		{
			if (Input.GetMouseButtonDown(0))
			{
				anim.SetBool("gameStarted", true);
				StartCoroutine(Example());
			}
		}
	}

	IEnumerator Example()
	{
		yield return new WaitForSeconds(1.0f);
		SceneManager.LoadScene(1, LoadSceneMode.Single);
	}

}
