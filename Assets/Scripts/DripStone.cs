﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class DripStone : MonoBehaviour
{
    [SerializeField] private AudioClip[] m_DripPool;
    [SerializeField] private AudioSource m_DripSource;
    private float m_NextDrip;

    private void Start()
    {
        m_NextDrip = Time.time + Random.Range(1f, 6f);
    }

    private void Update()
    {
        if (m_NextDrip < Time.time)
        {
            m_DripSource.clip = m_DripPool[Random.Range(0, m_DripPool.Length)];
            m_DripSource.pitch = Random.Range(0.8f, 1.2f);
            m_DripSource.Play();
            m_NextDrip = Time.time + Random.Range(m_DripSource.clip.length + 5, 20f);
        }
    }
}